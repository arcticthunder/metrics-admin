import React, {useState } from 'react';

import {
  TextField,
  Divider,
  Typography,
  makeStyles,
  Button,
  LinearProgress,
  Fade,
} from '@material-ui/core';
import {connect} from 'react-redux';
import {Redirect} from 'react-router-dom';
import {createUser as createUserAction} from '../../Redux/actions/userActions';

const useStyles = makeStyles (theme => ({
  container: {
    display: 'flex',
    flexWrap: 'wrap',
  },
  textField: {
    marginLeft: theme.spacing (1),
    marginRight: theme.spacing (1),
  },
}));

const RegisterTextField = props => {
  const {name, label, type, classes, handleChange, ...other} = props;
  return (
    <TextField
      id={`${name}Input`}
      label={label}
      name={name}
      type={type}
      margin="normal"
      variant="outlined"
      fullWidth
      onChange={handleChange}
    />
  );
};

const Register = props => {

  const [email, setEmail] = useState('')
  const [username, setUsername] = useState('')
  const [password, setPassword] = useState('')
  const [verifyPassword, setVerifyPassword] = useState('')
  const [canLogin, setCanLogin] = useState(false)

  const createUser = props.createUser

  const handleChange = (event) => {
    switch (event.target.name) {
      case 'usrName':
        setUsername(event.target.value)
        setCanLogin(event.target.value !== '' && email !== '' && password !== '' && verifyPassword !== '' && password === verifyPassword)
        break;

      case 'email':
        setEmail(event.target.value)
        setCanLogin(username !== '' && event.target.value !== '' && password !== '' && verifyPassword !== '' && password === verifyPassword)
        break;

      case 'password':
        setPassword(event.target.value)
        setCanLogin(username !== '' && email !== '' && event.target.value !== '' && verifyPassword !== '' && event.target.value === verifyPassword)
        break;

      case 'verifyPassword':
        setVerifyPassword(event.target.value)
        setCanLogin(username !== '' && email !== '' && password !== '' && event.target.value !== '' && password === event.target.value)
        break;

      default:
        console.log (`Error: ${event.target.name}`);
        break;
    }
  }

  const handleSubmit = ( event ) => {
    event.preventDefault();
    createUser(username, email, password)
  }

  const renderRedirect = () => {
    if (props.isSuccess) {
      return <Redirect to="/dashboard" />;
    }
  };

  return (
    <div>
      {renderRedirect()}
      <Typography variant="h5" align="left">Set Up Your Account</Typography>
      <Divider />
      <RegisterTextField
        name="usrName"
        label="Organization/User Name"
        classes={useStyles.textField}
        handleChange={handleChange}
      />
      <RegisterTextField
        name="email"
        label="Email"
        classes={useStyles.textField}
        handleChange={handleChange}
      />
      <RegisterTextField
        name="password"
        label="Password"
        type="password"
        classes={useStyles.textField}
        handleChange={handleChange}
      />
      <RegisterTextField
        name="verifyPassword"
        label="Verify"
        type="password"
        classes={useStyles.textField}
        handleChange={handleChange}
      />
      <Button
        color="primary"
        variant="contained"
        disabled={!canLogin}
        onClick={handleSubmit}
      >
        Register
      </Button>

      <Fade
        in={props.loading}
        style={{
          transitionDelay: props.loading ? '800ms' : '0ms',
        }}
        unmountOnExit
      >
        <LinearProgress />
      </Fade>
    </div>
  );
}

const mapStateToProps = state => {
  const {user} = state;
  return {
    user: user.info,
    loading: user.loading,
    error: user.error,
    isSuccess: user.error === null && user.info.email !== undefined
  };
}

const mapDispatchToProps = dispatch => ({
  createUser (username, email, password) {
    dispatch (createUserAction (username, email, password));
  },
})

export default connect (mapStateToProps, mapDispatchToProps) (Register);
