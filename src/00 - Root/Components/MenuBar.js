import React, { useState } from 'react';
import { Link as RouterLink } from 'react-router-dom';
import { connect } from 'react-redux';
import { logOutUser as logOutUserAction } from '../../Redux/actions/userActions';
import {
  makeStyles,
  AppBar,
  Toolbar,
  Typography,
  Button,
  Link,
  IconButton,
  Menu,
  MenuItem,
} from '@material-ui/core';

import { Equalizer, AccountCircle } from '@material-ui/icons';

const useStyles = makeStyles(theme => ({
  root: {
    display: 'flex',
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
  },
  menuButton: {
    marginRight: theme.spacing(2),
  },
  login: {
    textTransform: "none",
  },
  title: {
    flexGrow: 1,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
  toolbar: theme.mixins.toolbar,
}));

export const UserAccountMenu = props => {
  const {isAuthenticated} = props
  const [anchorEl, setAnchorEl] = useState(null);
  const isMenuOpen = Boolean(anchorEl);

  const handleProfileMenuOpen = event => {
    setAnchorEl(event.currentTarget);
  }

  const handleMenuClose = event => {
    setAnchorEl(null);
  }

  const handleLogout = event => {
    if (isAuthenticated) {
      props.logOutUser();
    }
    handleMenuClose()
  };
  const menuId = 'primary-search-account-menu';

  const renderMenu = (
    <Menu
      anchorEl={anchorEl}
      anchorOrigin={{ vertical: 'top', horizontal: 'right' }}
      id={menuId}
      keepMounted
      transformOrigin={{ vertical: 'top', horizontal: 'right' }}
      open={isMenuOpen}
      onClose={handleMenuClose}
    >
      <MenuItem onClick={handleMenuClose}>Profile</MenuItem>
      <MenuItem onClick={handleLogout}>Logout</MenuItem>
    </Menu>
  );

  return (
    <div>
      <IconButton
        edge="end"
        aria-label="account of current user"
        aria-controls={menuId}
        aria-haspopup="true"
        onClick={handleProfileMenuOpen}
        color="inherit"
      >
        <AccountCircle />
      </IconButton>
      {renderMenu}
    </div>
  )
}

export const MenuBar = props => {
  const classes = useStyles();
  const {isAuthenticated, user} = props
  const logOutUser = () => props.logOutUser()
  const renderButton = () => {
    if(isAuthenticated) {
      return <UserAccountMenu isAuthenticated={isAuthenticated}/>
    } else {
      return <Button variant="outlined" color="inherit" className={classes.login}> Login or Register </Button>
    }
  }
  return (
    <AppBar position="fixed" className={classes.appBar}>
      <Toolbar>
        <Typography variant="h6" className={classes.menuButton}>
          <Equalizer />
        </Typography>
        <Typography align="left" variant="h6" className={classes.title}>
          Arctic Thunder Metrics
        </Typography>
        <Link
          color="inherit"
          underline="none"
          to={`/login`}
          component={RouterLink}
        >
          {renderButton()}
        </Link>
      </Toolbar>
    </AppBar>
  );
};

// Link Redux user object to dialog
const mapStateToProps = state => {
  const { user } = state;
  return {
    user: user.info,
    isAuthenticated: user.info.token !== undefined,
  };
};

const mapDispatchToProps = dispatch => ({
  logOutUser() {
    dispatch(logOutUserAction());
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(MenuBar);
