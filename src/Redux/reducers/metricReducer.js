import {metric as types} from '../actions/actionTypes';
import initialState from './initialState';

const removeMetric = (arr, target) => {
  const index = arr.findIndex (metric => metric.equals (target));
  return arr.splice (index, 1);
};

export default function metricReducer (state = initialState.metrics, action) {
  switch (action.type) {
    // Get all metrics
    case types.GET_METRICS_LOADING:
    case types.CREATE_METRIC_LOADING:
    case types.DELETE_METRIC_LOADING:
      return Object.assign ({}, state, {loading: true});

    case types.GET_METRICS_FAILURE:
    case types.CREATE_METRIC_FAILURE:
    case types.DELETE_METRIC_FAILURE:
      return Object.assign ({}, state, {
        loading: false,
        error: action.payload.error,
      });

    case types.GET_METRICS_SUCCESS:
      return Object.assign (
        {},
        state,
        {
          loading: false,
          error: null,
        },
        action.payload.metrics
      );

    case types.CREATE_METRIC_SUCCESS:
      return Object.assign (
        {},
        state,
        {
          loading: false,
          error: null,
        },
        {
          count: state.count + 1,
          next: null,
          previous: null,
          results: [action.payload.metric, ...state.results],
        }
      );

    case types.DELETE_METRIC_SUCCESS:
      return Object.assign (
        {},
        state,
        {
          loading: false,
          error: null,
        },
        {
          count: state.count - 1,
          next: null,
          previous: null,
          results: removeMetric (state.results, action.payload.metric),
        }
      );

    default:
      return state;
  }
}
