import {combineReducers} from 'redux';
import user from './userReducer';
import projects from './projectReducer';
import metrics from './metricReducer';
import currentPage from './pageChangeReducer';

const rootReducer = combineReducers ({
  user,
  projects,
  metrics,
  currentPage,
});

export default rootReducer;
