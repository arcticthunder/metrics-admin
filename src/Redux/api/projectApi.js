import runtimeEnv from '@mars/heroku-js-runtime-env'
const env = runtimeEnv()
class ProjectApi {
  // Gets all projects
  static getProjects (token, limit, offset) {
    return fetch (
      `${env.REACT_APP_API_HOST}/projects/?limit=${limit}&offset=${offset}`,
      {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Token ${token}`,
        },
      }
    )
      .then (response => {
        return response.json ();
      })
      .catch (error => {
        return error;
      });
  }

  static getProject (token, id) {
    return fetch (`${env.REACT_APP_API_HOST}/projects/${id}/`, {
      method: 'GET',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Token ${token}`,
      },
    })
      .then (response => {
        return response.json ();
      })
      .catch (error => {
        return error;
      });
  }

  // Creates a new project
  static createProject (token, name, description) {
    return fetch (`${env.REACT_APP_API_HOST}/projects/`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Token ${token}`,
      },
      body: JSON.stringify ({
        name: name,
        description: description,
      }),
    })
      .then (response => {
        return response.json ();
      })
      .catch (error => {
        return error;
      });
  }

  static updateProject (token, id, name, description) {
    return fetch (`${env.REACT_APP_API_HOST}/projects/${id}/`, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Token ${token}`,
      },
      body: JSON.stringify ({
        name: name,
        description: description,
      }),
    })
      .then (response => {
        return response.json ();
      })
      .catch (error => {
        return error;
      });
  }
  // Delete a project
  static deleteProject (token, id) {
    return fetch (`${env.REACT_APP_API_HOST}/projects/${id}/`, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Token ${token}`,
      },
    })
      .then (response => {
        return response.json ();
      })
      .catch (error => {
        return error;
      });
  }
}

export default ProjectApi;
