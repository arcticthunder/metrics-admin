import runtimeEnv from '@mars/heroku-js-runtime-env'
const env = runtimeEnv()
class UserApi {
    static logInUser( username, password ) {
        return fetch(`${env.REACT_APP_API_HOST}/login/`, {
            method: 'POST',
            body: JSON.stringify({
              username: username,
              password: password
            }),
            headers: {
              'Content-Type': 'application/json'
            }
        })
        .then(response => response.json()) 
        .then(data => {
            return (
                data
            )
        })
        .catch(error => { return error })
    }

    static createUser( username, email, password ) {
        return fetch(`${env.REACT_APP_API_HOST}/new_user/`, {
            method: 'POST',
            body: JSON.stringify({
              username: username,
              email: email,
              password: password
            }),
            headers: {
              'Content-Type': 'application/json'
            }
        })
        .then(response => response.json()) 
        .then(data => {
            return (
                data
            )
        })
        .catch(error => { return error })
    }
}

export default UserApi