import runtimeEnv from '@mars/heroku-js-runtime-env'
const env = runtimeEnv()
class MetricApi {
  //Get all metrics for project
  static getMetrics (token, pid, limit = 100, offset = 0) {
    return fetch (
      `${env.REACT_APP_API_HOST}/projects/${pid}/metrics/?limit=${limit}&offset=${offset}`,
      {
        method: 'GET',
        headers: {
          'Content-Type': 'application/json',
          Authorization: `Token ${token}`,
        },
      }
    )
      .then (response => {
        return response.json ();
      })
      .catch (error => {
        return error;
      });
  }

  // Delete specific metric
  static deleteMetric (token, pid, mid) {
    return fetch (`${env.REACT_APP_API_HOST}/projects/${pid}/metrics/${mid}`, {
      method: 'DELETE',
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Token ${token}`,
      },
    })
      .then (response => {
        return response.json ();
      })
      .catch (error => {
        return error;
      });
  }
}

export default MetricApi;
