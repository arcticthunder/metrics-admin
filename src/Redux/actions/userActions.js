import userApi from '../api/userApi.js';
import {user as type} from './actionTypes';

export const logInUser = (username, password) => {
  return (dispatch, getState) => {
    dispatch (logInLoading ());

    userApi
      .logInUser (username, password)
      .then (userData => {
        console.log (userData);
        dispatch (logInSuccess (userData));
      })
      .catch (error => {
        dispatch (logInFailure (error));
        throw error;
      });
  };
};

export const logInLoading = () => {
  return {
    type: type.LOG_IN_LOADING,
  };
};

export const logInFailure = error => {
  return {
    type: type.LOG_IN_FAILURE,
    payload: {error},
  };
};

export const logInSuccess = userData => {
  return {
    type: type.LOG_IN_SUCCESS,
    payload: {userData},
  };
};

export const logOutUser = () => {
  return (dispatch, getState) => {
    dispatch (logOutSuccess ());
  };
};

export const logOutSuccess = () => {
  return {type: type.LOG_OUT_SUCCESS};
};

export const createUser = (username, email, password) => {
  return (dispatch, getState) => {
    dispatch (createUserLoading ());

    userApi
      .createUser (username, email, password)
      .then (userData => {
        dispatch (createUserSuccess (userData));
      })
      .catch (error => {
        dispatch (createUserFailure (error));
        throw error;
      });
  };
};

export const createUserLoading = () => {
  return {
    type: type.CREATE_USER_LOADING,
  };
};

export const createUserFailure = error => {
  return {
    type: type.CREATE_USER_FAILURE,
    payload: {error},
  };
};

export const createUserSuccess = userData => {
  return {
    type: type.CREATE_USER_SUCCESS,
    payload: {userData},
  };
};
