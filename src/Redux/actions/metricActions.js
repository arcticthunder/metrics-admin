import {metric as types} from './actionTypes';
import metricsApi from '../api/metricApi';

// Get All Metrics
export const getMetrics = (pid, limit, offset) => (dispatch, getState) => {
  dispatch (getMetricsLoading ());
  metricsApi
    .getMetrics (getToken (getState), pid, limit, offset)
    .then (metrics => {
      dispatch (getMetricsSuccess (metrics));
    })
    .catch (error => {
      dispatch (getMetricsFailure (error));
    });
};

const getMetricsLoading = () => {
  return {
    type: types.GET_METRICS_LOADING,
  };
};

const getMetricsFailure = error => {
  return {
    type: types.GET_METRICS_FAILURE,
    payload: {error},
  };
};

const getMetricsSuccess = metrics => {
  return {
    type: types.GET_METRICS_SUCCESS,
    payload: {metrics},
  };
};

// Create Metric
export const createMetric = () => (dispatch, getState) => {
  dispatch (createMetricLoading ());
  metricsApi
    .createMetric (getToken (getState))
    .then (metric => {
      dispatch (createMetricSuccess (metric));
    })
    .catch (error => {
      dispatch (createMetricFailure (error));
    });
};

const createMetricLoading = () => {
  return {
    type: types.CREATE_METRIC_LOADING,
  };
};

const createMetricFailure = error => {
  return {
    type: types.CREATE_METRIC_FAILURE,
    payload: {error},
  };
};

const createMetricSuccess = metric => {
  return {
    type: types.CREATE_METRIC_SUCCESS,
    payload: {metric},
  };
};

// Delete Metric
export const deleteMetric = id => (dispatch, getState) => {
  dispatch (deleteMetricLoading ());
  metricsApi
    .deleteMetric (getToken (getState), id)
    .then (metric => {
      dispatch (deleteMetricSuccess (metric));
    })
    .catch (error => {
      dispatch (deleteMetricFailure (error));
    });
};

const deleteMetricLoading = payload => {
  return {
    type: types.DELETE_METRIC_LOADING,
  };
};

const deleteMetricFailure = error => {
  return {
    type: types.DELETE_METRIC_FAILURE,
    payload: {error},
  };
};

const deleteMetricSuccess = metric => {
  return {
    type: types.DELETE_METRIC_SUCCESS,
    payload: {metric},
  };
};

const getToken = getState => {
  return getState ().user.info.token;
};
