import projectApi from '../api/projectApi';
import {project as types} from './actionTypes';

const getToken = getState => {
  return getState ().user.info.token;
};

// Get all projects
export const getProjects = (limit, offset) => {
  return (dispatch, getState) => {
    dispatch (getProjectsLoading ());

    projectApi
      .getProjects (getToken(getState), limit, offset)
      .then (projects => {
        dispatch (getProjectsSuccess (projects));
      })
      .catch (error => {
        dispatch (getProjectsFailure (error));
      });
  };
};

export const getProjectsLoading = () => {
  return {
    type: types.GET_PROJECTS_LOADING,
  };
};

export const getProjectsFailure = error => {
  return {
    type: types.GET_PROJECTS_FAILURE,
    payload: {error},
  };
};
export const getProjectsSuccess = projects => {
  return {
    type: types.GET_PROJECTS_SUCCESS,
    payload: {projects},
  };
};

// Get specific project
export const getProject = (id) => {
  return (dispatch, getState) => {
    dispatch (getProjectLoading ());

    projectApi
      .getProject (getToken(getState), id)
      .then (project => {
        dispatch (getProjectSuccess (project));
      })
      .catch (error => {
        dispatch (getProjectFailure (error));
      });
  };
};

export const getProjectLoading = () => {
  return {
    type: types.GET_PROJECT_LOADING,
  };
};

export const getProjectFailure = error => {
  return {
    type: types.GET_PROJECT_FAILURE,
    payload: {error},
  };
};
export const getProjectSuccess = project => {
  return {
    type: types.GET_PROJECT_SUCCESS,
    payload: {project},
  };
};

// Create new project
export const createProject = (name, description) => {
  return (dispatch, getState) => {
    dispatch (createProjectLoading ());

    projectApi
      .createProject (getToken (getState), name, description)
      .then (project => {
        dispatch (getProjectSuccess (project));
      })
      .catch (error => {
        dispatch (createProjectFailure (error));
      });
  };
};

export const createProjectLoading = () => {
  return {
    type: types.CREATE_PROJECT_LOADING,
  };
};

export const createProjectFailure = error => {
  return {
    type: types.CREATE_PROJECT_FAILURE,
    payload: {error},
  };
};

export const createProjectSuccess = project => {
  return {
    type: types.CREATE_PROJECT_SUCCESS,
    payload: {project},
  };
};

// Update a project
export const updateProject = (id, name, description) => (dispatch, getState) => {
  dispatch(updateProjectLoading())
  projectApi.updateProject(getToken(getState), id, name, description)
  .then(project => {
    dispatch(updateProjectSuccess(project))
  })
  .catch(error => {
    dispatch(updateProjectFailure(error))
  })
}

const updateProjectLoading = () => ({
  type: types.UPDATE_PROJECT_LOADING,
})

const updateProjectSuccess = project => ({
  type: types.UPDATE_PROJECT_SUCCESS,
  payload: {project}
})

const updateProjectFailure = error => ({
  type: types.UPDATE_PROJECT_FAILURE,
  payload: {error}
})

// Delete a project
export const deleteProject = id => {
  return (dispatch, getState) => {
    dispatch (deleteProjectLoading ());

    projectApi
      .deleteProject (getToken (getState), id)
      .then (project => {
        dispatch (deleteProjectSuccess (project));
      })
      .catch (error => {
        dispatch (deleteProjectFailure (error));
      });
  };
};

export const deleteProjectLoading = () => ({
  type: types.DELETE_PROJECT_LOADING,
});

export const deleteProjectFailure = error => ({
  type: types.DELETE_PROJECT_FAILURE,
  payload: {error},
});

export const deleteProjectSuccess = project => ({
  type: types.DELETE_PROJECT_SUCCESS,
  payload: {project},
});
