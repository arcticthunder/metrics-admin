import React, {forwardRef, useEffect, useState} from 'react';

import {
  Paper,
  Typography,
  makeStyles,
  TablePagination,
} from '@material-ui/core';

import {
  AddBox,
  ArrowUpward,
  Check,
  ChevronLeft,
  ChevronRight,
  Clear,
  DeleteOutline,
  Edit,
  FilterList,
  FirstPage,
  LastPage,
  Remove,
  SaveAlt,
  Search,
  ViewColumn,
} from '@material-ui/icons';
import MaterialTable from 'material-table';
import {connect} from 'react-redux';
import {
  getMetrics as getMetricsAction,
} from '../../../Redux/actions/metricActions';

const useStyles = makeStyles (theme => ({
  button: {
    margin: theme.spacing (1),
  },
  input: {
    display: 'none',
  },
  fab: {
    margin: theme.spacing (1),
  },
  codeBlock: {
    textTransform: 'none',
    fontFamily: ['Courier'],
  },
}));

const metricDetail = (data, classes) => {
  return (
    <div>
      <Paper>
        {renderDataRow (classes, data)}
      </Paper>
    </div>
  );
};

const MetricsTable = props => {
  const classes = useStyles ();
  const {metrics, count, isLoading, pid} = props;
  const getMetrics = (numPerRow, pageNum) =>
    props.getMetrics (pid, numPerRow, numPerRow * pageNum);

  const [columns, setColumns] = useState ([
    {title: 'Time', field: 'timestamp', type: 'datetime'},
    {title: 'Id', field: 'id'},
  ]);
  const [numRows, setNumRows] = useState (5);
  const [page, setPage] = useState (0);

  const updateColumnsForData = () => {
    var initial = [
      {title: 'Time', field: 'timestamp', type: 'datetime'},
      {title: 'Id', field: 'id'}, 
    ]

    var keys = ['timestamp', 'id']
    metrics.map( (metric, index) => {
      Object.keys (metric.data).forEach ((key, index) => {
        if(keys.find(function(element) {
          return element === key
        }) === undefined) {
          keys.push(key)
          initial.push({title: key, field: `data.${key}`})
        }
      })
    })

    setColumns(initial)
  }

  const handlePageChange = (event, newPage) => {
    setPage (newPage);
  };

  const handlePageSizeChange = event => {
    setNumRows (parseInt (event.target.value, 10));
    setPage (0);
  };

  useEffect (() => {
    getMetrics (numRows, page);
    updateColumnsForData()
  }, [numRows, page]);

  return (
    <MaterialTable
      title={`Metrics`}
      columns={columns}
      data={metrics}
      components={{
        Pagination: () => (
          <TablePagination
            rowsPerPageOptions={[5, 10, 25]}
            component="div"
            count={count}
            rowsPerPage={numRows}
            page={page}
            onChangePage={handlePageChange}
            onChangeRowsPerPage={handlePageSizeChange}
          />
        ),
      }}
      options={{
        search: false,
        pageSize: numRows,
      }}
      detailPanel={[
        {
          tooltip: 'Metric Details',
          render: rowData => {
            return metricDetail (rowData.data, classes);
          },
        },
      ]}
      onRowClick={(event, rowData, togglePanel) => togglePanel ()}
      icons={tableIcons}
      isLoading={isLoading}
    />
  );
};

const tableIcons = {
  Add: forwardRef ((props, ref) => <AddBox {...props} ref={ref} />),
  Check: forwardRef ((props, ref) => <Check {...props} ref={ref} />),
  Clear: forwardRef ((props, ref) => <Clear {...props} ref={ref} />),
  Delete: forwardRef ((props, ref) => <DeleteOutline {...props} ref={ref} />),
  DetailPanel: forwardRef ((props, ref) => (
    <ChevronRight {...props} ref={ref} />
  )),
  Edit: forwardRef ((props, ref) => <Edit {...props} ref={ref} />),
  Export: forwardRef ((props, ref) => <SaveAlt {...props} ref={ref} />),
  Filter: forwardRef ((props, ref) => <FilterList {...props} ref={ref} />),
  FirstPage: forwardRef ((props, ref) => <FirstPage {...props} ref={ref} />),
  LastPage: forwardRef ((props, ref) => <LastPage {...props} ref={ref} />),
  NextPage: forwardRef ((props, ref) => <ChevronRight {...props} ref={ref} />),
  PreviousPage: forwardRef ((props, ref) => (
    <ChevronLeft {...props} ref={ref} />
  )),
  ResetSearch: forwardRef ((props, ref) => <Clear {...props} ref={ref} />),
  Search: forwardRef ((props, ref) => <Search {...props} ref={ref} />),
  SortArrow: forwardRef ((props, ref) => <ArrowUpward {...props} ref={ref} />),
  ThirdStateCheck: forwardRef ((props, ref) => <Remove {...props} ref={ref} />),
  ViewColumn: forwardRef ((props, ref) => <ViewColumn {...props} ref={ref} />),
};

const renderDataRow = (classes, data, level = 0) => {
  var tabs = [];
  for (var i = 0; i < level; i++) {
    tabs.push ('----- ');
  }
  return Object.keys (data).map ((key, index) => {
    return (
      <div>
        <Typography className={classes.codeBlock} key={level + index}>
          {tabs}{key}: {typeof data[key] !== 'object' ? data[key] : ''}
        </Typography>
        {typeof data[key] === 'object'
          ? renderDataRow (data[key], level + 1)
          : ''}
      </div>
    );
  });
};

const mapStateToProps = (state, ownProps) => {
  const {metrics} = state;

  return {
    count: metrics.count,
    metrics: metrics.results,
    isLoading: metrics.loading,
    error: metrics.error,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    getMetrics: (pid, limit, offset) =>
      dispatch (getMetricsAction (pid, limit, offset)),
  };
};

export default connect (mapStateToProps, mapDispatchToProps) (MetricsTable);
