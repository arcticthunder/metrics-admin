import React, {forwardRef, useEffect, useState} from 'react';
import {
  Typography,
  Divider,
  Grid,
  Fab,
  Grow,
  Tooltip,
  TextField,
} from '@material-ui/core';

import {
  Cancel,
  Delete,
  Edit,
  Save,
} from '@material-ui/icons';

import {makeStyles, useTheme} from '@material-ui/core/styles';
import {red, green} from '@material-ui/core/colors';

import {connect} from 'react-redux';
import {Redirect} from 'react-router-dom';
import {
  getProject as getProjectAction,
} from '../../Redux/actions/projectActions';
import {
  deleteProject as deleteProjectAction,
} from '../../Redux/actions/projectActions';

import ConfirmDeleteDialog from './Components/ConfirmDeleteDialog';
import MetricsTable from './Components/MetricsTable';

const useStyles = makeStyles (theme => ({
  button: {
    margin: theme.spacing (1),
  },
  input: {
    display: 'none',
  },
  fab: {
    margin: theme.spacing (1),
  },
  codeBlock: {
    textTransform: 'none',
    fontFamily: ['Courier'],
  },
}));

export const ProjectDetailPage = props => {
  const {project, project_id} = props;
  const getProject = id => {
    props.getProject (id);
  };
  const deleteProject = props.deleteProject;

  const classes = useStyles ();
  const theme = useTheme ();

  useEffect (() => {
    if (project === undefined) {
      getProject (project_id);
    }
  }, []);

  const {name, description} = project !== undefined
    ? project
    : {name: '', description: ''};

  const [newName, setNewName] = useState (name);
  const [newDesc, setNewDesc] = useState (description);
  const [editOpen, setEditOpen] = useState (false);
  const [saveNeeded, setSaveNeeded] = useState (false);
  const [wantsDelete, setWantsDelete] = useState (false);
  const [confirmDelete, setConfirmDelete] = useState (false);
  const [projectDeleted, setProjectDeleted] = useState (false);

  const transitionDuration = {
    enter: theme.transitions.duration.enteringScreen,
    exit: theme.transitions.duration.leavingScreen,
  };

  if (confirmDelete) {
    deleteProject (project_id);
  }

  const handleEditPress = () => {
    setEditOpen (!editOpen);
  };

  const handleSavePress = () => {
    setSaveNeeded (false);
    setEditOpen (false);
  };

  const handleDeletePress = () => {
    setWantsDelete (true);
  };

  const handleChange = event => {
    event.target.id === 'name'
      ? setNewName (event.target.value)
      : setNewDesc (event.target.description);
    setSaveNeeded (newName !== name || newDesc !== description);
  };

  const handleClose = () => {
    setWantsDelete (!wantsDelete);
  };

  const handleContinue = () => {
    setWantsDelete (!wantsDelete);
    setConfirmDelete (true);
  };

  const renderRedirect = () => {
    if (projectDeleted) {
      return <Redirect to="/projects/" />;
    }
  };

  const renderFabs = () => {
    return (
      <div>
        {fabs.editBar.map ((fab, index) => (
        <Grow
          key={fab.color}
          in={fab.initial || editOpen}
          timeout={transitionDuration}
          unmountOnExit
        >
          <Tooltip title={fab.label}>
            <Fab
              aria-label={fab.label}
              id={fab.label.toLowerCase ()}
              className={fab.className}
              size={fab.size}
              style={{
                backgroundColor: fab.color,
              }}
              onClick={fab.onClick}
            >
              {fab.icon}
            </Fab>
          </Tooltip>
        </Grow>
      ))}
      </div>
    )
  }

  const fabs = {
    editBar: [
      {
        color: red[500],
        className: classes.fab,
        icon: <Delete />,
        label: 'Delete',
        size: 'small',
        initial: false,
        onClick: handleDeletePress,
      },
      {
        className: classes.fab,
        icon: <Save />,
        label: 'Save',
        size: 'small',
        initial: false,
        color: saveNeeded ? green[500] : null,
        onClick: handleSavePress,
      },
      {
        color: 'primary',
        className: classes.fab,
        icon: editOpen ? <Cancel /> : <Edit />,
        label: editOpen ? 'Cancel' : 'Edit',
        size: 'medium',
        initial: true,
        onClick: handleEditPress,
      },
    ],
  };

  const renderEditableText = () => {
    if (editOpen) {
      return (
        <div>
          <TextField
            margin="dense"
            id="name"
            label="Project Name"
            fullWidth
            onChange={handleChange}
            placeholder={name}
          />
          <TextField
            margin="dense"
            id="description"
            label="Project Description"
            fullWidth
            onChange={handleChange}
            placeholder={description}
          />
        </div>
      );
    } else {
      return (
        <div>
          <Typography variant="h4" align="left">{name}</Typography>
          <Typography variant="body1" align="left">{description}</Typography>
          <Typography variant="body1" align="left">id: {project.id}</Typography>
        </div>
      );
    }
  };

  return (
    <section className="project-detail">
      {renderRedirect ()}
      <Grid container spacing={4} direction="column" justify="flex-start">
        <Grid
          container
          item
          direction="row"
          justify="space-between"
          alignItems="center"
        >
          <Grid item>
            {renderEditableText ()}
          </Grid>
          <Grid item>
            {renderFabs()}
            <ConfirmDeleteDialog
              title="Delete Project"
              description="Are you sure you want to delete this project? This operation cannot be undone"
              open={wantsDelete}
              onClose={handleClose}
              onContinue={handleContinue}
            />
          </Grid>
        </Grid>
        <Grid item>
          <Divider />
        </Grid>
        <Grid item>
          <MetricsTable pid={project_id}/>
        </Grid>
      </Grid>
    </section>
  );
};

const mapStateToProps = (state, ownProps) => {
  const { projects } = state;

  return {
    project: projects.results.find (element => {
      return element.id == ownProps.project_id;
    }),
    loading: projects.loading,
    error: projects.error,
  }
};

const mapDispatchToProps = dispatch => {
  return {
    getProject: id => dispatch (getProjectAction (id)),
    deleteProject: id => dispatch (deleteProjectAction (id)),
  };
};

export default connect (mapStateToProps, mapDispatchToProps) (
  ProjectDetailPage
);
