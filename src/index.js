import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import * as serviceWorker from './serviceWorker';
import {Provider} from 'react-redux';
import configureStore from './Redux/store/configureStore';
import runtimeEnv from '@mars/heroku-js-runtime-env'

const store = configureStore ();
const unsubscribe = store.subscribe (() => console.log (store.getState ()));
const env = runtimeEnv();
console.log(`MODE: ${env.NODE_ENV}`)
console.log(`URL: ${env.REACT_APP_API_HOST}`)
ReactDOM.render (
  <Provider store={store}>
    <App />
  </Provider>,
  document.getElementById ('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister ();
